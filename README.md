# LabWork

## About this Project

[LabWork](https://gitlab.com/jayo/labwork) is an experimental attempt at creating an ongoing
journal/notebook/scratchpad/worklog/weeknotes/omg/wtf/bbq related to the work I do as a Support
Engineer for GitLab.

Inspired by all the git-based notebooks I've seen through the years, and the work of
my colleagues in GitLab - and the perfect storm of wanting some transparent and historical
record of all the verbosity - here we are with "LabWork" : The One Project to Note Them All

## Organization

I fully expect my organizational scheme will be like America in Terrence Mann's soliquy from _Field of Dreams_
It's going to get "erased like a blackboard, rebuilt, and erased again."

[Issue #1](#1) is the tracking issue for developing a first go at

For now - the core elements are going to be [Issues](https://gitlab.com/jayo/labwork/-/issues) collated together with
[Labels](https://gitlab.com/jayo/labwork/-/labels) - and [Merge Requests](https://gitlab.com/jayo/labwork/-/merge_requests)
for the repository itself. While I'm likely the only one posting here - the Merge Request workflow is something I've used
in all my coding work - and it's the model that [GitLab uses for the Handbook](https://about.gitlab.com/handbook/handbook-usage/)

## Other Projects and Resources

* [Glops](https://gitlab.com/jayo/glops):  Glops is the core project that I use for my [test environments](testing/index.md)
* [Yaks](https://gitlab.com/jasonadamyoung/yaks): Yaks is my note-taking project for personal work

## Credits

Hat tip to @mlockhart, my Support Engineer colleague whose shared his model of using GitLab for work notes with our
team right as I was looking for something to improve my note taking and sharing workflow.
